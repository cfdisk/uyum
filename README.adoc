= uyum

uyum ist ein Bash-Skript für RHEL/CentOS/Fedora um beliebige Pakete lokal aus yum/dnf Repos als nicht-root zu installieren. 

Über eine private yum/dnf.conf können weitere Repos eingebunden werden.



== Installation
[source,shell]
----
cd $HOME/bin
wget https://gitlab.com/cfdisk/uyum/raw/master/uyum
chmod +x uyum
----

In $HOME/.bashrc (oder $HOME/.zshrc) hinzufügen:

`export PATH=$HOME/local/usr/bin/:$HOME/local/usr/sbin/:$PATH` 

== Je nach Anwendungen: weiter Pfade hinzufügen. Beispiele:

- LD_LIBRARY_PATH: whenever a programm can't find its required libs:
`export LD_LIBRARY_PATH=$HOME/local/usr/lib/...`

- TCL libs:
`export TCLLIBPATH=$HOME/local/usr/lib64/tcl8.5/tls1.6.7`

- eggdrop language path:
`EGG_LANGDIR=$HOME/local/usr/share/eggdrop/language/`

Probably more to come...

== Notes

uyum lässt sich über eine eigene $HOME/local/etc/yum.conf bzw. $HOME/local/etc/dnf/dnf.conf konfigurieren. Solange diese nicht vorhanden ist, wird Systemstandard /etc/yum.conf bzw.  /etc/dnf/dnf.conf verwendet.

== Verwendung:

`uyum search <paketname>`

`uyum install <paketname>`

`uyum update <paketname>`

`uyum list`

`uyum erase <paketname>`

== Beispiele:
----
uyum search telnet
uyum install telnet
uyum update telnet
uyum list
uyum erase telnet
----


== Todo

upgrade für alle Pakete noch nicht möglich. Funktioneirt nur für einzelne Pakete 
